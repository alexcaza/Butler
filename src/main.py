from downloader import Downloader
from file_manager import FileManager
from installer import Installer
import os
import json
import sys


class ButlerMain:

    def main(self, downList):
        # instatiate classes
        fm = FileManager()
        dl = Downloader()
        inst = Installer()
        arguments = downList

        # Deletes temp folder incase ones
        # already created in the downloads directory
        fm.deleteFolder()
        # Create the temp folder
        fm.createFolder()

        dlPath = "/Users/" + os.getlogin() + "/Downloads/temp/"

        with open('data.json') as dataFile:
            data = json.load(dataFile)

        for i in range(0, len(arguments)):
            # Download the file(s)
            # dl.getFile(sublimeText['url'], sublimeText['fileName'], dlPath)
            if data[arguments[i]]['type'] == "dmg":
                print "\nDMG File\n"
                dl.getFile(
                    data[arguments[i]]['url'], data[arguments[i]]['fileName'], dlPath)

            elif data[arguments[i]]['type'] == "pkg":
                print "\nPackage File\n"

                dl.getFile(
                    data[arguments[i]]['url'], data[arguments[i]]['fileName'], dlPath)
            elif data[arguments[i]]['type'] == "zip":

                print "\nZIP File\n"
                dl.getFile(
                    data[arguments[i]]['url'], data[arguments[i]]['fileName'], dlPath)
                # dl.getFile(atom['url'], atom['fileName'], dlPath)
            # print type(arguments[i])
            # Install the applications
            # inst.installDMG(sublimeText['name'], "Sublime\ Text")

        # Cleanup the temp files
        # fm.deleteFolder()

# if __name__ == "__main__":
#     try:
#         main()
#     except KeyboardInterrupt:
#         print "Quitting Application Cleanly"
#         try:
#             sys.exit(0)
#         except SystemExit:
#             sys.exit(0)
