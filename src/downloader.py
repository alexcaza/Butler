import urllib2


class Downloader:

    # Get the file from the URL specified with the FileName specified
    def getFile(self, url, fileName, downloadPath):

        url = url
        downloadPath = downloadPath + fileName
        u = urllib2.urlopen(url)
        f = open(downloadPath, 'wb')
        meta = u.info()
        fileSize = int(meta.getheaders("Content-Length")[0])
        print "Downloading: %s Bytes: %s" % (fileName, fileSize)

        fileSizeDl = 0
        blockSize = 8192
        while True:
            buffer = u.read(blockSize)
            if not buffer:
                break

            fileSizeDl += len(buffer)
            f.write(buffer)
            # status = r"%10d  [%3.2f%%]" % (fileSizeDl, fileSizeDl * 100. / fileSize)
            # status = status + chr(8)*(len(status)+1)
            # print status,

        f.close()
