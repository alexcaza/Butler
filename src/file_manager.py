import os
import shutil


class FileManager:
    # Create temp folder in User's Downloads folder
    def createFolder(self):
        os.mkdir("/Users/" + os.getlogin() + "/Downloads/temp/")

    # Delete temp folder recursively
    def deleteFolder(self):
        try:
            shutil.rmtree("/Users/" + os.getlogin() + "/Downloads/temp/")
            pass
        except OSError:
            print "Folder Deleted"
            pass
