import os


class Installer:

    # Open DMG file, move NAME.app over to Applications folder,
    # unmount DMG file

    def installDMG(self, fileName, volumeName):
        os.system("hdiutil mount -nobrowse -quiet /Users/"
                  + os.getlogin() + "/Downloads/temp/" + fileName +
                  "&& cp -R /Volumes/" + volumeName +
                  "/*.app ~/Desktop/testDir " +
                  "&& hdiutil unmount /Volumes/" + volumeName)

    def installPKG(self, fileName):
        os.system("sudo installer -allowUntrusted -pkg /Users/"
                  + os.getlogin() + "/Downloads/temp/" + fileName)

    def installZIP(self, fileName, zipName, outFolder, folderName):
        if outFolder is "No":
            os.system("unzip /Users/" + os.getlogin() +
                      "/Downloads/temp/"
                      + zipName + ".zip && mv "
                      + fileName.split('.')[0] + ".app /Applications")
        elif outFolder is "Yes":
            os.system("unzip /Users/" + os.getlogin() +
                      "/Downloads/temp/"
                      + zipName + ".zip && cd " + folderName + "mv "
                      + fileName.split('.')[0] + ".app /Applications")
