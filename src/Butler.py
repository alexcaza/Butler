from Cocoa import *
from Foundation import *
from main import ButlerMain


class ButlerMainController(NSWindowController):

    itemTag = objc.IBOutlet()


    def windowDidLoad(self):
        NSWindowController.windowDidLoad(self)

        self.textField = 0
        self.downloadableArray = []
        self.Butler = ButlerMain()

    @objc.IBAction
    def isChecked_(self, sender):
        print "Sender Title: {0}, Sender Tag: {1}".format(sender.title(), sender.tag())
        if sender.state() == 1:
            self.downloadableArray.append(sender.title())
        elif sender.state() == 0:
            for i in range(0, len(self.downloadableArray)):
                if self.downloadableArray[i] == sender.title():
                    self.downloadableArray.pop(i)
                    return
        print "List of items: {0}".format(self.downloadableArray)

    @objc.IBAction
    def downloadFiles_(self, sender):
        self.Butler.main(self.downloadableArray)

if __name__ == "__main__":
    app = NSApplication.sharedApplication()

    viewController = ButlerMainController.alloc().initWithWindowNibName_(
        "Butler")

    viewController.showWindow_(viewController)

    NSApp.activateIgnoringOtherApps_(True)

    from PyObjCTools import AppHelper
    AppHelper.runEventLoop()
